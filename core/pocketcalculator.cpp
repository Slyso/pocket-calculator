#include "pocketcalculator.h"
#include "sevensegment.h"
#include "calc.h"

#include <sstream>
#include <string>
#include <exception>

void pocketcalculator(std::istream &in, std::ostream &out) {
    std::string line{};

    while (std::getline(in, line)) {
        try {
            std::istringstream s{ line };
            printLargeNumber(calc(s), out);
        } catch (std::exception const &exception) {
            out << exception.what() << '\n';
            printLargeError(out);
        }
    }
}
