#pragma once

#include <iosfwd>

int calc(int left, int right, char operation);

int calc(std::istream &in);
